---
layout: docs
title: Overview
permalink: /docs/kubeflow
style_class: kubeflow
---

### What is Kubeflow?
[Kubeflow](https://www.kubeflow.org/) is an open source AI/ML project focused on model training, serving, pipelines, and metadata. As part of the Open Data Hub project we worked on enabling Kubeflow 1.0 on Openshift 4.4. The following Kubeflow components are included in the installation

#### Components
- Central Dashboard
- Jupyterhub
- Katib
- Pipelines
- Distributed Training: Pytorch, tf-jobs
- Serving: Seldon
- Istio


{% include next-link.html label="Installation" url="/docs/kubeflow/installation.html" %}
